import React from "react";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    title: {
      textAlign: 'center'
    },
    card: {
      marginTop: '20px',
      marginBottom: '20px' 
  },
  text: {
    marginBottom: '5px'
},
media: {
    height: '250px',
    width: '100%'
  }
  })
);

export default function CustomCard(props: any) {
  const classes = useStyles();
  return (
      <Grid item xs={12}>
        <h2 className={classes.title}>List of Restaurants</h2>
        <Grid container spacing={3}>
          {props.restaurantData.restaurants.map((field: any) => (
            <Grid item xs={12} lg={3} md={3}>
              <Card variant="outlined" className={classes.card}>
                <CardMedia
                  component="img"
                  className={classes.media}
                  src={
                    field?.restaurant.featured_image
                      ? field?.restaurant.featured_image
                      : "https://b.zmtcdn.com/data/res_imagery/6113798_RESTAURANT_1410863975_d55a911d3706a5c18deecc59efe83947_c.jpg"
                  }
                  title="image"
                />
                <CardContent>
                  <Typography
                    variant="h5"
                    component="h2"
                    className={classes.text}
                  >
                    {field?.restaurant.name}
                  </Typography>

                  <Typography
                    variant="body2"
                    component="p"
                    className={classes.text}
                  >
                    {`Address : ${field?.restaurant.location.address}`}
                  </Typography>

                  <Typography
                    variant="body2"
                    component="p"
                    className={classes.text}
                  >
                    {`GeoLocation: ${field?.restaurant.location.latitude} , ${field?.restaurant.location.longitude}`}
                  </Typography>

                  <Typography
                    variant="body2"
                    component="p"
                    className={classes.text}
                  >
                    {`Locality: ${field?.restaurant.location.locality}`}
                  </Typography>

                  <Typography
                    variant="body2"
                    component="p"
                    className={classes.text}
                  >
                    {`Average cost for two person: ${field?.restaurant.currency} ${field?.restaurant.average_cost_for_two}`}
                  </Typography>
                </CardContent>
              </Card>
            </Grid>
          ))}
        </Grid>
      </Grid>
  );
}
