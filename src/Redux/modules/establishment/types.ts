export const ESTABLISHMENT_DETAILS_REQUEST_START: string = 'establishment/details/REQUEST_START';
export const ESTABLISHMENT_DETAILS_REQUEST_FAIL: string = 'establishment/details/REQUEST_FAIL';
export const ESTABLISHMENT_DETAILS_REQUEST_SUCCESS: string = 'establishment/details/REQUEST_SUCCESS';