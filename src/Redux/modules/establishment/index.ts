import * as actions from './actions';
import * as types from './types';
import { reducer } from './reducer';

export {
  actions,
  reducer,
  types
};

export default reducer;
