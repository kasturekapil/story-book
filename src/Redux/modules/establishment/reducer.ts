import {
    ESTABLISHMENT_DETAILS_REQUEST_FAIL,
    ESTABLISHMENT_DETAILS_REQUEST_START,
    ESTABLISHMENT_DETAILS_REQUEST_SUCCESS
    } from './types';

    import { EstablishmentAction }from './actions';

    type EstablishmentDetailsState = {
        loadingState: string,
        result: any
      };
    
      const INITIAL_STATE: EstablishmentDetailsState = {
        loadingState: 'UNINITIALISED',
        result: null
      };
    
      export const reducer = (
        state = INITIAL_STATE,
        action: EstablishmentAction
      ): EstablishmentDetailsState => {
        switch (action.type) {
          case ESTABLISHMENT_DETAILS_REQUEST_START:
            return {
              ...state,
              loadingState: 'LOADING'
            };
      
          case ESTABLISHMENT_DETAILS_REQUEST_SUCCESS:
            return {
              ...state,
              loadingState: 'LOADED',
              result: action.payload
            };
      
          case ESTABLISHMENT_DETAILS_REQUEST_FAIL:
            return {
              ...state,
              loadingState: 'ERROR'
            };
    
          default:
            return state;
        }
      };