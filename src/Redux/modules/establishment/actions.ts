import {
    ESTABLISHMENT_DETAILS_REQUEST_FAIL,
    ESTABLISHMENT_DETAILS_REQUEST_START,
    ESTABLISHMENT_DETAILS_REQUEST_SUCCESS
} from './types';

import { get } from './../session/actions';

const ESTABLISHMENT_DEATILS_URL: Function = (cityid: number, lat: number, lon: number) => (`/establishments?city_id=${cityid}&lat=${lat}&lon=${lon}`);

export const establishmentDetailsRequestStart = () => ({
    type: ESTABLISHMENT_DETAILS_REQUEST_START
});

export const establishmentDetailsRequestFail = () => ({
    type: ESTABLISHMENT_DETAILS_REQUEST_FAIL
});

export const establishmentDetailsRequestSuccess = (payload: any) => ({
    type: ESTABLISHMENT_DETAILS_REQUEST_SUCCESS,
    payload
});

export const getEstablishmentData = (cityid: number, lat: number, lon: number) => (
    dispatch: Function
) => new Promise((resolve, reject) => {
    dispatch(establishmentDetailsRequestStart());
    dispatch(get(ESTABLISHMENT_DEATILS_URL(cityid, lat, lon))
    ).then((response: any) => {
        dispatch(establishmentDetailsRequestSuccess(response.data));
        resolve(response.data);
    }).catch((error: any) => {
        dispatch(establishmentDetailsRequestFail());
        reject();
    });
});

export type EstablishmentAction = {
    type: string,
    payload?: any,
}