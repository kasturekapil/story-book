import { combineReducers } from 'redux';

import { reducer as city } from './city';
import { reducer as cuisine } from './cuisine';
import { reducer as establishment } from './establishment';
import { reducer as geocodes } from './geocodes';
import { reducer as restaurant } from './restaurant';

const reducer = combineReducers({
    city,
    cuisine,
    establishment,
    geocodes,
    restaurant
});

export const rootReducer = (state, action) => {
    return reducer(state, action);
}

export type ReducerState = ReturnType<typeof rootReducer>;