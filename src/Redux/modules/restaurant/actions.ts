import {
    RESTAURANT_DETAILS_REQUEST_FAIL,
    RESTAURANT_DETAILS_REQUEST_START,
    RESTAURANT_DETAILS_REQUEST_SUCCESS
    } from './types';

import { get } from './../session/actions';

const RESTAURANT_DEATILS_URL: Function = (cityid: number, lat: number, lon: number, establishmentid: number, cuisineid: number) => 
(`/search?city_id=${cityid}&lat=${lat}&lon=${lon}&establishment_id=${establishmentid}&cuisines=${cuisineid}&radius=500&sort=real_distance&order=asc&start=0&count=20`);

export const restaurantDetailsRequestStart = () => ({
    type: RESTAURANT_DETAILS_REQUEST_START
});

export const restaurantDetailsRequestFail = () => ({
    type: RESTAURANT_DETAILS_REQUEST_FAIL
});

export const restaurantDetailsRequestSuccess = (payload: any) => ({
    type: RESTAURANT_DETAILS_REQUEST_SUCCESS,
    payload
});

export const getRestaurantData = (cityid: number, lat: number, lon: number, establishmentid: number, cuisineid: any) => (
    dispatch: Function
) => new Promise((resolve, reject) => {
    dispatch(restaurantDetailsRequestStart());
    dispatch(get(RESTAURANT_DEATILS_URL(cityid, lat, lon, establishmentid, cuisineid))
    ).then((response: any) => {
        dispatch(restaurantDetailsRequestSuccess(response.data));
        resolve(response.data);
    }).catch((error: any) => {
        dispatch(restaurantDetailsRequestFail());
        reject();
    });
});

export type RestaurantAction = {
    type: string,
    payload?: any,
}