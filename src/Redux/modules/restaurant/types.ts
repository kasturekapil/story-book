export const RESTAURANT_DETAILS_REQUEST_START: string = 'restaurant/details/REQUEST_START';
export const RESTAURANT_DETAILS_REQUEST_FAIL: string = 'restaurant/details/REQUEST_FAIL';
export const RESTAURANT_DETAILS_REQUEST_SUCCESS: string = 'restaurant/details/REQUEST_SUCCESS';