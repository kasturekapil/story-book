import {
    RESTAURANT_DETAILS_REQUEST_FAIL,
    RESTAURANT_DETAILS_REQUEST_START,
    RESTAURANT_DETAILS_REQUEST_SUCCESS
    } from './types';


    import { RestaurantAction }from './actions';

    type RestaurantDetailsState = {
        loadingState: string,
        result: any
      };
    
      const INITIAL_STATE: RestaurantDetailsState = {
        loadingState: 'UNINITIALISED',
        result: null
      };
    
      export const reducer = (
        state = INITIAL_STATE,
        action: RestaurantAction
      ): RestaurantDetailsState => {
        switch (action.type) {
          case RESTAURANT_DETAILS_REQUEST_START:
            return {
              ...state,
              loadingState: 'LOADING'
            };
      
          case RESTAURANT_DETAILS_REQUEST_SUCCESS:
            return {
              ...state,
              loadingState: 'LOADED',
              result: action.payload
            };
      
          case RESTAURANT_DETAILS_REQUEST_FAIL:
            return {
              ...state,
              loadingState: 'ERROR'
            };
    
          default:
            return state;
        }
      };