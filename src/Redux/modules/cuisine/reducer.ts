import {
    CUISINE_DETAILS_REQUEST_FAIL,
    CUISINE_DETAILS_REQUEST_START,
    CUISINE_DETAILS_REQUEST_SUCCESS
    } from './types';

import { CuisineAction }from './actions';

type CuisineDetailsState = {
    loadingState: string,
    result: any
  };

  const INITIAL_STATE: CuisineDetailsState = {
    loadingState: 'UNINITIALISED',
    result: null
  };

  export const reducer = (
    state = INITIAL_STATE,
    action: CuisineAction
  ): CuisineDetailsState => {
    switch (action.type) {
      case CUISINE_DETAILS_REQUEST_START:
        return {
          ...state,
          loadingState: 'LOADING'
        };
  
      case CUISINE_DETAILS_REQUEST_SUCCESS:
        return {
          ...state,
          loadingState: 'LOADED',
          result: action.payload
        };
  
      case CUISINE_DETAILS_REQUEST_FAIL:
        return {
          ...state,
          loadingState: 'ERROR'
        };

      default:
        return state;
    }
  };