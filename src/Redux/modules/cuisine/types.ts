export const CUISINE_DETAILS_REQUEST_START: string = 'cuisine/details/REQUEST_START';
export const CUISINE_DETAILS_REQUEST_FAIL: string = 'cuisine/details/REQUEST_FAIL';
export const CUISINE_DETAILS_REQUEST_SUCCESS: string = 'cuisine/details/REQUEST_SUCCESS';