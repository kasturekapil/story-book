import {
CUISINE_DETAILS_REQUEST_FAIL,
CUISINE_DETAILS_REQUEST_START,
CUISINE_DETAILS_REQUEST_SUCCESS
} from './types';

import { get } from './../session/actions';

const  CUISINE_DEATILS_URL: Function = (cityid: number, lat: number, lon: number) => (`/cuisines?city_id=${cityid}&lat=${lat}&lon=${lon}`);

export const cuisineDetailsRequestStart = () => ({
    type: CUISINE_DETAILS_REQUEST_START
  });
  
  export const cuisineDetailsRequestFail = () => ({
    type: CUISINE_DETAILS_REQUEST_FAIL
  });
  
  export const cuisineDetailsRequestSuccess = (payload: any) => ({
    type: CUISINE_DETAILS_REQUEST_SUCCESS,
    payload
  });

  export const getCuisineData = (cityid: number, lat: number, lon: number) => (
	dispatch: Function
) => new Promise((resolve, reject) => {
	dispatch(cuisineDetailsRequestStart());
	dispatch(get(CUISINE_DEATILS_URL(cityid, lat, lon))
	).then((response: any) => {
        dispatch(cuisineDetailsRequestSuccess(response.data));
        resolve(response.data);
	}).catch((error: any) => {
		dispatch(cuisineDetailsRequestFail());
		reject();
	});
});

export type CuisineAction = {
    type: string,
    payload?: any,
}