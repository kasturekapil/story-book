import {
    GEOCODE_DETAILS_REQUEST_FAIL,
    GEOCODE_DETAILS_REQUEST_START,
    GEOCODE_DETAILS_REQUEST_SUCCESS
    } from './types';

  import axios from 'axios';

const  GEOCODE_DEATILS_URL: Function = (city: string, key: string) =>
 (`http://dev.virtualearth.net/REST/v1/Locations?q=${city}&key=${key}`);

export const geocodeDetailsRequestStart = () => ({
    type: GEOCODE_DETAILS_REQUEST_START
  });
  
  export const geocodeDetailsRequestFail = () => ({
    type: GEOCODE_DETAILS_REQUEST_FAIL
  });
  
  export const geocodeDetailsRequestSuccess = (payload: any) => ({
    type: GEOCODE_DETAILS_REQUEST_SUCCESS,
    payload
  });

  export const getGeocodeData = (city: string, key: string) => (
	dispatch: Function
) => new Promise((resolve, reject) => {
	dispatch(geocodeDetailsRequestStart());
	axios.get(GEOCODE_DEATILS_URL(city, key)
	).then((response: any) => {
        dispatch(geocodeDetailsRequestSuccess(response.data.resourceSets));
        resolve(response.data.resourceSets);
	}).catch((error: any) => {
		dispatch(geocodeDetailsRequestFail());
		reject();
	});
});

export type GeocodeAction = {
    type: string,
    payload?: any,
}