export const GEOCODE_DETAILS_REQUEST_START: string = 'geocode/details/REQUEST_START';
export const GEOCODE_DETAILS_REQUEST_FAIL: string = 'geocode/details/REQUEST_FAIL';
export const GEOCODE_DETAILS_REQUEST_SUCCESS: string = 'geocode/details/REQUEST_SUCCESS';