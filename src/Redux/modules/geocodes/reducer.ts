import {
    GEOCODE_DETAILS_REQUEST_FAIL,
    GEOCODE_DETAILS_REQUEST_START,
    GEOCODE_DETAILS_REQUEST_SUCCESS
    } from './types';

    import { GeocodeAction }from './actions';

type GeocodeDetailsState = {
    loadingState: string,
    result: any
  };

  const INITIAL_STATE: GeocodeDetailsState = {
    loadingState: 'UNINITIALISED',
    result: null
  };

  export const reducer = (
    state = INITIAL_STATE,
    action: GeocodeAction
  ): GeocodeDetailsState => {
    switch (action.type) {
      case GEOCODE_DETAILS_REQUEST_START:
        return {
          ...state,
          loadingState: 'LOADING'
        };
  
      case GEOCODE_DETAILS_REQUEST_SUCCESS:
        return {
          ...state,
          loadingState: 'LOADED',
          result: action.payload
        };
  
      case GEOCODE_DETAILS_REQUEST_FAIL:
        return {
          ...state,
          loadingState: 'ERROR'
        };

      default:
        return state;
    }
  };