export const CITY_DETAILS_REQUEST_START: string = 'city/details/REQUEST_START';
export const CITY_DETAILS_REQUEST_FAIL: string = 'city/details/REQUEST_FAIL';
export const CITY_DETAILS_REQUEST_SUCCESS: string = 'city/details/REQUEST_SUCCESS';