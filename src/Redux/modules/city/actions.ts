import {
	CITY_DETAILS_REQUEST_FAIL,
    CITY_DETAILS_REQUEST_START,
    CITY_DETAILS_REQUEST_SUCCESS
} from './types';

import { get } from './../session/actions';

const  CITY_DEATILS_URL: Function = (city: string) => (`/cities?q=${city}`);

export const cityDetailsRequestStart = () => ({
    type: CITY_DETAILS_REQUEST_START
  });
  
  export const cityDetailsRequestFail = () => ({
    type: CITY_DETAILS_REQUEST_FAIL
  });
  
  export const cityDetailsRequestSuccess = (payload: any) => ({
    type: CITY_DETAILS_REQUEST_SUCCESS,
    payload
  });

  export const getCityData = (city: string) => (
	dispatch: Function
) => new Promise((resolve, reject) => {
	dispatch(cityDetailsRequestStart());
	dispatch(get(CITY_DEATILS_URL(city))
	).then((response: any) => {
        dispatch(cityDetailsRequestSuccess(response.data));
        resolve(response.data);
	}).catch((error: any) => {
		dispatch(cityDetailsRequestFail());
		reject();
	});
});

export type CityAction = {
    type: string,
    payload?: any,
}