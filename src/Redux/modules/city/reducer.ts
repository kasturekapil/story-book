import {
	CITY_DETAILS_REQUEST_FAIL,
    CITY_DETAILS_REQUEST_START,
    CITY_DETAILS_REQUEST_SUCCESS
} from './types';

import { CityAction }from './actions';

type CityDetailsState = {
    loadingState: string,
    result: any
  };

  const INITIAL_STATE: CityDetailsState = {
    loadingState: 'UNINITIALISED',
    result: null
  };

  export const reducer = (
    state = INITIAL_STATE,
    action: CityAction
  ): CityDetailsState => {
    switch (action.type) {
      case CITY_DETAILS_REQUEST_START:
        return {
          ...state,
          loadingState: 'LOADING'
        };
  
      case CITY_DETAILS_REQUEST_SUCCESS:
        return {
          ...state,
          loadingState: 'LOADED',
          result: action.payload
        };
  
      case CITY_DETAILS_REQUEST_FAIL:
        return {
          ...state,
          loadingState: 'ERROR'
        };

      default:
        return state;
    }
  };