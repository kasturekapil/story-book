import axios from "axios";

export const request = (config: any) => (dispatch: Function) =>
  new Promise((resolve, reject) => {
    axios({
      ...config,
      headers: {
        "Access-Control-Allow-Origin": "*",
        "user-key":  `ebce07efec80d0017e1a3ee7173cdbd6`,
        "Content-Type": "application/json",
      },
    })
      .then((response: any) => {
        resolve(response);
      })
      .catch((error: any) => {
          reject(error);
      });
  });

  export const get = (url: string) => (dispatch: Function) =>
  dispatch(
    request({
      url,
      method: "GET"
    })
  );