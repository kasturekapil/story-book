import React from "react";
import ReactDOM from 'react-dom';
import "@testing-library/jest-dom/extend-expect";
import { shallow } from "enzyme";
import App from "../App";
import { cleanup } from "@testing-library/react";
import renderer from 'react-test-renderer';

afterEach(cleanup);

describe("App component test", () => {
  it("Renders correctly in DOM", () => {
    shallow(<App />);
  });

  it("Renders it without crashing", () => {
      const div = document.createElement("div");
      ReactDOM.render(<App> </App>, div);
      ReactDOM.unmountComponentAtNode(div);
    });

    it("Generate snapshot successfully",()=>{
          const result = renderer.create(<App />).toJSON();
          expect(result).toMatchSnapshot();
    });
});
