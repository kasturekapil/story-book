import React from 'react';
import {BrowserRouter as Router,
  Switch,
  Route } from 'react-router-dom';
import MainContainer  from '../Containers/MainContainer';

function App() {
  return (
    <div className="App">
      <Router >
         <Switch>
            <Route exact path="/" component={MainContainer} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
