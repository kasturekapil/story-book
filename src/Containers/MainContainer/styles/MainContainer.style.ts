import { createStyles } from '@material-ui/core/styles';
export default () => createStyles({
	mainContainer: {
        padding: '20px',
        display: 'flex'
    },
    inputField: {
        height: '20px',
        width: '100%',
        paddingBottom: '40px'
    },
    button: {
       height: '60px',
       width: '100%'
    }
});
