import React, { Component } from 'react';
import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';
import { withStyles, WithStyles } from '@material-ui/core/styles';
import { actions as CityAction } from '../../Redux/modules/city';
import { actions as CuisineAction } from '../../Redux/modules/cuisine';
import { actions as EstblishmentAction } from '../../Redux/modules/establishment';
import { actions as GeocodeAction } from '../../Redux/modules/geocodes';
import { actions as RestaurantAction } from '../../Redux/modules/restaurant';
import { ReducerState } from '../../Redux/modules';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import MainContainerStyles from './styles/MainContainer.style';
import Grid from '@material-ui/core/Grid';
import CustomCard from '../../Component/atoms/CustomCard';

interface MainContainerProps extends WithStyles<typeof MainContainerStyles> {
	getCityData: Function,
  getGeocodeData: Function,
  getEstablishmentData: Function,
  getCuisineData: Function,
  getRestaurantData: Function,
  cityData: any,
  geocodeData: any,
  establishmentData: any,
  cuisineData: any,
  restaurantData: any
}

interface MainContainerStates {
	city: string,
  showData: boolean,
  isEstablishmentGetCalled: boolean,
  isCuisineGetCalled: boolean,
  isGeocodeGetCalled: boolean,
  isCityGetCalled: boolean
}

class MainContainer extends Component<MainContainerProps, MainContainerStates> {
  constructor(props: MainContainerProps) {
    super(props);
    this.state = {
		city: '',
    showData: false,
    isEstablishmentGetCalled: false,
    isCuisineGetCalled: false,
    isCityGetCalled: false,
    isGeocodeGetCalled: false
	};
	this.onCityEntered = this.onCityEntered.bind(this); 
  this.onGetCategory = this.onGetCategory.bind(this);
  this.onGetRestaurants = this.onGetRestaurants.bind(this);
  }

  onCityEntered(event) {
    const {
      getGeocodeData,
      getCityData
    } = this.props;

    const GEO_MAP_KEY = 'AhA8FMsAq-SMdHv4lEKAlTCfw6yQWW8OXelDRgO2qrR1sihtL9Agu06BMLKO5Hbt';
		this.setState({
			city: event.target.value
		},()=>{
      getCityData(this.state.city).then(()=>{
            this.setState({
              isCityGetCalled: true
          });
      })
      getGeocodeData(this.state.city, GEO_MAP_KEY).then(()=>{
            this.setState({
              isGeocodeGetCalled: true
          });
       })
    })
  }

  onGetCategory(){
      const {
        cityData,
        geocodeData,
        getEstablishmentData,
        getCuisineData
      } = this.props;

      const cityid = cityData?.location_suggestions[0].id,
      lat = geocodeData[0].resources[0].geocodePoints[0].coordinates[0] ,
      lon =  geocodeData[0].resources[0].geocodePoints[0].coordinates[1];

      getEstablishmentData(cityid,lat,lon).then(()=>{
            this.setState({
                isEstablishmentGetCalled: true
            });
      });
      getCuisineData(cityid,lat,lon).then(()=>{
          this.setState({
            isCuisineGetCalled: true,
          });
      });
  }

  onGetRestaurants() {
      const {
        cuisineData,
        establishmentData,
        cityData,
        geocodeData,
        getRestaurantData
      } = this.props;

      const cityid = cityData?.location_suggestions[0].id,
      lat = geocodeData[0].resources[0].geocodePoints[0].coordinates[0] ,
      lon =  geocodeData[0].resources[0].geocodePoints[0].coordinates[1],
      cuisineid = cuisineData?.cuisines[0].cuisine?.cuisine_id ,
      establishmentid = establishmentData?.establishments[0].establishment?.id;

      getRestaurantData(cityid,lat,lon,establishmentid,cuisineid).then(()=>{
            this.setState({
                  showData: true
            });
      });
  }



  render() {

    const {
     restaurantData,
     classes
    } = this.props;

    return (
      <div className={classes.mainContainer}>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <h2>Zomato App</h2>
          </Grid>
          <Grid item xs={12}>
            <TextField
              id="outlined-basic"
              label="Enter City"
              variant="outlined"
              onChange={this.onCityEntered}
              className={classes.inputField}
            />
          </Grid>
          <Grid item xs={12}>
            <Button
              variant="contained"
              color="primary"
              onClick={this.onGetCategory}
              className={classes.button}
              disabled={
                !this.state.isGeocodeGetCalled &&
                !this.state.isCityGetCalled
              }
            >
              Get Restaurants Catgory
            </Button>
          </Grid>
          <Grid item xs={12}>
            <Button
              variant="contained"
              color="primary"
              onClick={this.onGetRestaurants}
              className={classes.button}
              disabled={
                !this.state.isEstablishmentGetCalled &&
                !this.state.isCuisineGetCalled
              }
            >
              Get Restaurants
            </Button>
          </Grid>
          {this.state.showData && (
            <CustomCard restaurantData={restaurantData}/>
          )}
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = (state: ReducerState) => ({
	cityData: state.city.result,
  cuisineData: state.cuisine.result,
  establishmentData: state.establishment.result,
  geocodeData: state.geocodes.result,
  restaurantData: state.restaurant.result
});

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators({
	getCityData: CityAction.getCityData,
  getCuisineData: CuisineAction.getCuisineData,
  getEstablishmentData: EstblishmentAction.getEstablishmentData,
  getGeocodeData: GeocodeAction.getGeocodeData,
  getRestaurantData:RestaurantAction.getRestaurantData  
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(MainContainerStyles)(MainContainer));